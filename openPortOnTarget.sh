#!/usr/bin/env bash

TARGET_IP=192.168.56.123

if [ "$1" != "" ]; then
  TARGET_IP="$1"
fi

./check_target.sh $TARGET_IP
res=$?

if [ $res == 1 ]; then
  echo "Target $TARGET_IP is not present. Can't proceed."
  exit 1
fi

ssh $TARGET_IP "top | tee plik.txt"
ssh -f $TARGET_IP 'nc -l 4321 &'
nmap -p 4321 $TARGET_IP >/dev/null 2>&1
res=$?
if [ $res == 1 ]; then
  echo "Cannot connect to port 4321 on $TARGET_IP."
  exit 1
fi
echo "Successfully connected to $TARGET_IP on port 4321"
ssh $TARGET_IP 'pkill nc'
nmap -p 4321 $TARGET_IP >/dev/null 2>&1
res=$?
if [ $res == 1 ]; then
  echo "Cannot connect to port 4321 on $TARGET_IP."
  exit 1
fi
echo "Connected to port 4321"