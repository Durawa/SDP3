#!/usr/bin/env bash

TARGET_IP=192.168.56.123

if [ "$1" != "" ]; then
    TARGET_IP="$1"
fi

echo "Checking if target $TARGET_IP is present"
ping $TARGET_IP -c1 -t1 >/dev/null 2>&1
res=$?
if [ $res == 0 ]; then
    echo "Target is present"
else
    echo "Target is not present"
fi